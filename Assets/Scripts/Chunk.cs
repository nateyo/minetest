﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimplexNoise;

[RequireComponent (typeof(MeshRenderer))]
[RequireComponent (typeof(MeshCollider))]
[RequireComponent (typeof(MeshFilter))]
public class Chunk : MonoBehaviour 
{
	public static List<Chunk> chunks = new List<Chunk>();
	public static int width
	{
		get{ 
			return World.currentWorld.chunkWidth;
		}
	}
	public static int height
	{
		get{
			return World.currentWorld.chunkHeight;
		}
	}
	public static int numUVBricks
	{
		get{
			return World.currentWorld.numUVBricks;
		}
	}
	public byte[,,] map;
	public Mesh visualMesh;
	protected MeshRenderer meshRenderer;
	protected MeshCollider meshCollider;
	protected MeshFilter meshFilter;
	
	// Use this for initialization
	void Start () 
	{
		chunks.Add (this);
		meshRenderer = GetComponent<MeshRenderer>();
		meshCollider = GetComponent<MeshCollider>();
		meshFilter = GetComponent<MeshFilter>(); 
		
		StartCoroutine(CalculateMapFromScratch());
		StartCoroutine(CreateVisualMesh());
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public static byte GetTheoreticalByte(Vector3 pos)
	{
		Random.seed = World.currentWorld.seed;
		Vector3 grain0Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain1Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain2Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		
		return GetTheoreticalByte(pos, grain0Offset, grain1Offset, grain2Offset);
	}
	
	public static byte GetTheoreticalByte(Vector3 pos, Vector3 offset0, Vector3 offset1, Vector3 offset2)
	{
		float heightBase = 10;
		float maxHeight = height - 10;
		float heightSwing = maxHeight - heightBase;
		
		byte brick = 1;
		
		float mountainValue = CalculateNoiseValue(pos, offset0, 0.009f);
		//mountainValue = Mathf.Sqrt(mountainValue);
		mountainValue *= heightSwing;
		mountainValue += heightBase;
		mountainValue += (CalculateNoiseValue(pos, offset1, 0.05f) * 10) - 8;
		//mountainValue = Mathf.Pow(mountainValue, 3f); //Cool kind of caverns
		mountainValue = Mathf.Pow (mountainValue, 0.8f);
		
		if(mountainValue > pos.y)
			if(pos.y > 3 && pos.y <= 5)
				return (byte) 3;
			else if(pos.y > 5)
				return (byte) 5;
			else
				return brick;
		return 0;
	}
	
	public virtual IEnumerator CalculateMapFromScratch()
	{
		map = new byte[width, height, width];
		Random.seed = World.currentWorld.seed;
		Vector3 grain0Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain1Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		Vector3 grain2Offset = new Vector3(Random.value * 10000, Random.value * 10000, Random.value * 10000);
		
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++) 
			{
				for (int z = 0; z < width; z++) 
				{
					map[x,y,z] = GetTheoreticalByte(new Vector3(x, y, z) + transform.position);
				}
			}
		}
		
		yield return 0;
	}
	
	public static float CalculateNoiseValue(Vector3 pos, Vector3 offset, float scale)
	{
		float noiseX = Mathf.Abs((pos.x + offset.x) * scale);
		float noiseY = Mathf.Abs((pos.y + offset.y) * scale);
		float noiseZ = Mathf.Abs((pos.z + offset.z) * scale);
		
		return Mathf.Max(0, Noise.Generate(noiseX, noiseY, noiseZ));
	}
	
	public virtual IEnumerator CreateVisualMesh()
	{
		visualMesh = new Mesh();
		
		List<Vector3> verts = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> tris = new List<int>();
		
		for (int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				for (int z = 0; z < width; z++) 
				{
					if(map[x,y,z] == 0) continue;
					
					byte brick = map[x,y,z];
					if(IsTransparent(x - 1, y, z))
						BuildFace (brick, new Vector3(x, y, z), Vector3.up, Vector3.forward, false, verts, uvs, tris); //left
					if(IsTransparent(x + 1, y, z))
						BuildFace (brick, new Vector3(x + 1, y, z), Vector3.up, Vector3.forward, true, verts, uvs, tris); //right

					if(IsTransparent(x, y - 1, z))
						BuildFace (brick, new Vector3(x, y, z), Vector3.forward, Vector3.right, false, verts, uvs, tris); //bottom
					if(IsTransparent(x, y + 1, z))
						BuildFace (brick, new Vector3(x, y + 1, z), Vector3.forward, Vector3.right, true, verts, uvs, tris); //top
					
					if(IsTransparent(x, y, z - 1))
						BuildFace (brick, new Vector3(x, y, z), Vector3.up, Vector3.right, true, verts, uvs, tris); //back
					if(IsTransparent(x, y, z + 1))
						BuildFace (brick, new Vector3(x, y, z + 1), Vector3.up, Vector3.right, false, verts, uvs, tris); //front
				}
			}
		}
		
		visualMesh.vertices = verts.ToArray();
		visualMesh.uv = uvs.ToArray();
		visualMesh.triangles = tris.ToArray();
		visualMesh.RecalculateBounds();
		visualMesh.RecalculateNormals();
		
		meshFilter.mesh = visualMesh;
		meshCollider.sharedMesh = visualMesh;
		
		yield return 0;
	}
	
	public virtual void BuildFace(byte brick, Vector3 corner, Vector3 up, Vector3 right, bool reversed, List<Vector3> verts, List<Vector2> uvs, List<int> tris)
	{
		int index = verts.Count;
		verts.Add (corner);
		verts.Add (corner + up);
		verts.Add (corner + up + right);
		verts.Add (corner + right);
		
		float brickSize = 1f/(float)numUVBricks;
		float brickNum = (float)brick/(float)numUVBricks;
		
		Vector2 uvWidth = new Vector2( brickSize, brickSize);
		Vector2 uvCorner = new Vector2(0.0f,brickNum);
		
		if(reversed)
		{
			tris.Add (index + 0);
			tris.Add (index + 1);
			tris.Add (index + 2);
			tris.Add (index + 2);
			tris.Add (index + 3);
			tris.Add (index + 0);
			
			uvs.Add(new Vector2(uvCorner.x, uvCorner.y));
			uvs.Add(new Vector2(uvCorner.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y));
		}
		else
		{
			tris.Add (index + 1);
			tris.Add (index + 0);
			tris.Add (index + 2);
			tris.Add (index + 3);
			tris.Add (index + 2);
			tris.Add (index + 0);
			
			//TODO: These UV faces are backwards....
			uvs.Add(new Vector2(uvCorner.x, uvCorner.y));
			uvs.Add(new Vector2(uvCorner.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y));
		}
	}
	
	public virtual bool IsTransparent(int x, int y, int z)
	{
		if( y < 0 ) return false; //don't draw the bottom
		byte brick = GetByte(x, y, z);
		switch(brick)
		{
			default:
			case 0: return true;
			case 1: return false;
			case 3: return false;
			case 5: return false;
		}
	}
	
	public virtual byte GetByte(int x, int y, int z)
	{
		if( (y < 0) || (y >= height) )
			return 0;
		
		if( (x < 0) || (z < 0) || (x >= width) || (z >= width) )
		{
			Vector3 worldPos = new Vector3(x, y, z) + transform.position;
			Chunk chunk = Chunk.FindChunk(worldPos);
			if(chunk == this) return 0;
			if(chunk == null)
			{
				return GetTheoreticalByte(worldPos);
			}
			return chunk.GetByte(worldPos);
		}
		
		return map[x, y, z];
	}
	
	public virtual byte GetByte(Vector3 worldPos)
	{
		worldPos -= transform.position;
		int x = Mathf.FloorToInt(worldPos.x);
		int y = Mathf.FloorToInt(worldPos.y);
		int z = Mathf.FloorToInt(worldPos.z);
		
		return GetByte(x, y, z);
	}
	
	public static Chunk FindChunk(Vector3 pos)
	{
		for(int a = 0; a < chunks.Count; a++)
		{
			Vector3 cpos = chunks[a].transform.position;
			
			if( (pos.x < cpos.x) || (pos.z < cpos.z) || (pos.x  >= cpos.x + width) || (pos.z >= cpos.z + width) ) continue;
			return chunks[a];
		}
		return null;
	}
	
	public bool SetBrick (byte brick, Vector3 worldPos)
	{
		worldPos -= transform.position;
		return SetBrick(brick, Mathf.FloorToInt(worldPos.x), Mathf.FloorToInt(worldPos.y), Mathf.FloorToInt(worldPos.z));
	}
	
	public bool SetBrick(byte brick, int x, int y, int z)
	{
		if( ( x < 0 ) || ( y < 0 ) || ( z < 0 ) | (x >= width) || (y >= height) || (z >= width))
		{
			return false;
		}
		if(map[x,y,z] == brick) return false;
		map[x,y,z] = brick;
		StartCoroutine(CreateVisualMesh());
		
		if(x == 0)
		{
			Chunk chunk = FindChunk(new Vector3(x - 2, y, z) + transform.position);
			if(chunk != null)
				StartCoroutine(chunk.CreateVisualMesh());
		}
		if(x == width - 1)
		{
			Chunk chunk = FindChunk(new Vector3(x + 2, y, z) + transform.position);
			if(chunk != null)
				StartCoroutine(chunk.CreateVisualMesh());
		}
		if(z == 0)
		{
			Chunk chunk = FindChunk(new Vector3(x, y, z - 2) + transform.position);
			if(chunk != null)
				StartCoroutine(chunk.CreateVisualMesh());
		}
		if(z == width - 1)
		{
			Chunk chunk = FindChunk(new Vector3(x, y, z + 2) + transform.position);
			if(chunk != null)
				StartCoroutine(chunk.CreateVisualMesh());
		}
		return true;
	}
}
