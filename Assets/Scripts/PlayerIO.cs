﻿using UnityEngine;
using System.Collections;

public class PlayerIO : MonoBehaviour {
	public static PlayerIO currentPlayerIO;
	public float maxInteractionRange = 8f;
	public byte selectedInventory = 0;
	// Use this for initialization
	void Start () {
		currentPlayerIO = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{
			selectedInventory = 1;
			Debug.Log ("Click to remove blocks!");
		}
		else if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			selectedInventory = 2;
			Debug.Log ("Click to add blocks!");
		}
		else if (Input.GetKeyDown (KeyCode.Equals)) 
		{
			World.currentWorld.viewRange += 50;
			Debug.Log ("ViewRange: "+World.currentWorld.viewRange);
		}
		else if (Input.GetKeyDown (KeyCode.Minus)) 
		{
			if((World.currentWorld.viewRange-50) > 0)
			{
				World.currentWorld.viewRange -= 50;
			}
			Debug.Log ("ViewRange: "+World.currentWorld.viewRange);
		}

		if(Input.GetMouseButtonDown(0))
		{
			Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.5f));
			RaycastHit hit;
			
			if(Physics.Raycast(ray, out hit, maxInteractionRange))
			{
				Chunk chunk = hit.transform.GetComponent<Chunk>();
				if(chunk == null)
				{
					Debug.Log ("Clicked on " + hit.transform.name + " and it's not a chunk.");
					return;
				}
				Vector3 p = hit.point;
				
				switch(selectedInventory)
				{
					case 1:
						p -= hit.normal / 4;
						chunk.SetBrick(0, p);
						break;
					case 2:
						p += hit.normal / 4;
						chunk.SetBrick(1, p);
						break;
					default:
						break;
				}
			}
			else
			{
				Debug.Log("Clicked, but nothing was there!");
			}
		}
	}
}
